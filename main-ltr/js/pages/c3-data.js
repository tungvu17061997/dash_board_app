//[c3 charts Javascript]

//Project:	CrmX Admin - Responsive Admin Template
//Primary use:   Used only for the morris charts


$(function () {
    "use strict";

    var a = c3.generate({
        bindto: "#data-color",
        size: {
            height: 370
        },
        data: {
            columns: [
                ['Like', 30, 20, 50, 40, 60, 50],
                ['Chia sẻ', 200, 130, 90, 240, 130, 220],
                ['Xem video', 300, 200, 160, 400, 250, 250]
            ],
            type: "bar",
            colors: {
                data1: "#38649f",
                data2: "#389f99"
            },
            color: function (a, o) {
                return o.id && "data3" === o.id ? d3.rgb(a).darker(o.value / 150) : a
            }
        },
        grid: {
            y: {
                show: !0
            }
        }
    });



    // customized chart
    // ------------------------------
    // based on prepared DOM, initialize echarts instance
    var customizedChart = echarts.init(document.getElementById('customized-chart'));
    var option = {

        // backgroundColor: '#fff',
        

        title: {
            // text: 'Customized Pie',
            left: 'center',
            top: 20,
            textStyle: {
                color: '#ccc'
            }
        },

        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b} : {c} ({d}%)"
        },

        visualMap: {
            show: false,
            min: 80,
            max: 600,
            inRange: {
                colorLightness: [0, 1]
            }
        },
        series: [{
            name: 'Marketing',
            type: 'pie',
            radius: '55%',
            center: ['50%', '50%'],
            data: [{
                    value: 335,
                    name: 'Số bài chưa duyệt'
                },
                {
                    value: 310,
                    name: 'Số User đăng bài'
                },
                {
                    value: 274,
                    name: 'Số User mới đăng kí'
                },
                {
                    value: 235,
                    name: 'Số bài vi phạm'
                },
                {
                    value: 400,
                    name: 'Số bài đã duyệt'
                }
            ].sort(function (a, b) {
                return a.value - b.value;
            }),
            roseType: 'radius',
            label: {
                normal: {
                    textStyle: {
                        color: 'rgba(0, 0, 0, 0.3)'
                    }
                }
            },
            labelLine: {
                normal: {
                    lineStyle: {
                        color: 'rgba(0, 0, 0, 0.3)'
                    },
                    smooth: 0.2,
                    length: 10,
                    length2: 20
                }
            },
            itemStyle: {
                normal: {
                    color: '#38649f',
                    shadowBlur: 200,
                    shadowColor: 'rgba(0, 0, 0, 0.5)'
                }
            },

            animationType: 'scale',
            animationEasing: 'elasticOut',
            animationDelay: function (idx) {
                return Math.random() * 200;
            }
        }]
    };


    customizedChart.setOption(option);


    // ------------------------------
    // Nested chart
    // ------------------------------
    // based on prepared DOM, initialize echarts instance
    var nestedChart = echarts.init(document.getElementById('nested-pie'));
    var option = {

        tooltip: {
            trigger: 'item',
            formatter: "{a} <br/>{b}: {c} ({d}%)"
        },
         // Add legend
         legend: {
            orient: 'vertical',
            x: 'left',
            data: ['Phim ảnh', 'Âm nhạc', 'Thể thao', 'Ẩm thực', 'Khôi hài', 'Giải trí', 'Trò chơi', 'Chia sẽ', 'Bình luận','Yêu thích' ,'Khác']
        },

        // Add custom colors
        color: ['#689f38', '#38649f', '#389f99', '#ee1044', '#ff8f00'],

        // Display toolbox
        toolbox: {
            show: true,
            orient: 'vertical',
            feature: {
                mark: {
                    show: true,
                    title: {
                        mark: 'Markline switch',
                        markUndo: 'Undo markline',
                        markClear: 'Clear markline'
                    }
                },
                dataView: {
                    show: true,
                    readOnly: false,
                    title: 'View data',
                    lang: ['View chart data', 'Close', 'Update']
                },
                magicType: {
                    show: true,
                    title: {
                        pie: 'Switch to pies',
                        funnel: 'Switch to funnel',
                    },
                    type: ['pie', 'funnel']
                },
                restore: {
                    show: true,
                    title: 'Restore'
                },
                saveAsImage: {
                    show: true,
                    title: 'Same as image',
                    lang: ['Save']
                }
            }
        },

        // Enable drag recalculate
        calculable: false,

        // Add series
        series: [

            // Inner
            {
                name: 'Countries',
                type: 'pie',
                selectedMode: 'single',
                radius: [0, '40%'],

                // for funnel
                x: '15%',
                y: '7.5%',
                width: '40%',
                height: '85%',
                funnelAlign: 'right',
                max: 1548,

                itemStyle: {
                    normal: {
                        label: {
                            position: 'inner'
                        },
                        labelLine: {
                            show: false
                        }
                    },
                    emphasis: {
                        label: {
                            show: true
                        }
                    }
                },

                data: [{
                        value: 535,
                        name: 'Bình luận'
                    },
                    {
                        value: 679,
                        name: 'Yêu thích'
                    },
                    {
                        value: 1548,
                        name: 'Chia sẻ'
                    }
                ]
            },

            // Outer
            {
                name: 'Countries',
                type: 'pie',
                radius: ['60%', '85%'],

                // for funnel
                x: '55%',
                y: '7.5%',
                width: '35%',
                height: '85%',
                funnelAlign: 'left',
                max: 1048,

                data: [{
                        value: 335,
                        name: 'Phim ảnh'
                    },
                    {
                        value: 310,
                        name: 'Âm nhạc'
                    },
                    {
                        value: 234,
                        name: 'Thể thao'
                    },
                    {
                        value: 135,
                        name: 'Ẩm thực'
                    },
                    {
                        value: 1048,
                        name: 'Khôi hài'
                    },
                    {
                        value: 251,
                        name: 'Giải trí'
                    },
                    {
                        value: 147,
                        name: 'Trò chơi'
                    },
                    {
                        value: 102,
                        name: 'Khác'
                    }
                ]
            }
        ]
    };


    nestedChart.setOption(option);

});